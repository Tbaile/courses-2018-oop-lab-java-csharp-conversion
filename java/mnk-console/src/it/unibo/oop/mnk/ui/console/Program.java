package it.unibo.oop.mnk.ui.console;

import it.unibo.oop.mnk.MNKMatch;
import it.unibo.oop.mnk.ui.console.control.MNKConsoleControl;
import it.unibo.oop.mnk.ui.console.view.MNKConsoleView;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Program {

    private static List<Integer> parseArgs(final List<String> argList) {
        return Stream.of("-m", "-n", "-k")
                .mapToInt(argList::indexOf)
                .mapToObj(i -> i >= 0 ? argList.get(i + 1) : "3")
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    public static void main(String... args) throws IOException {
        final List<Integer> params = parseArgs(Arrays.asList(args));
        final MNKMatch match = MNKMatch.of(params.get(0), params.get(1), params.get(2));
        MNKConsoleView.of(match);
        final MNKConsoleControl ctrl = MNKConsoleControl.of(match);

        match.reset();
        while (true) {
            ctrl.input();
        }
    }
}
